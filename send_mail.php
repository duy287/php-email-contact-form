<?php

    use PHPMailer\PHPMailer\PHPMailer;

    if (isset($_POST['name']) && isset($_POST['email'])) {

        // Get data from contact form
        $name = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $body = $_POST['body'];

        //Thêm các thư viện của Mailer
        require_once "PHPMailer/PHPMailer.php";
        require_once "PHPMailer/SMTP.php";
        require_once "PHPMailer/Exception.php";

        $mail = new PHPMailer();

        //SMTP Settings
        $mail->isSMTP();
        $mail->CharSet="utf-8";
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = "test@gmail.com";
        $mail->Password = 'test';
        $mail->Port = 587; 
        $mail->SMTPSecure = "tls"; 

        //Email Settings
        $mail->isHTML(true);
        $mail->setFrom("duylehoang1802@gmail.com", 'Duy Master'); // Người gửi
        $mail->addAddress($email); // Email nhận
        $mail->Subject = $subject; // Title mail
        $mail->Body = file_get_contents('template_mail.html');

        if ($mail->send()) {
            $status = "success";
            $response = "Email is sent!";
        } else {
            $status = "failed";
            $response = "Something is wrong: " . $mail->ErrorInfo;
        }

        exit(json_encode(array("status" => $status, "response" => $response)));
    }
?>
